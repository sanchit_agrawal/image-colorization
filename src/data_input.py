import numpy as np
from scipy.misc import imread
from scipy.misc import imresize
import os

def get_images(data_dir, batch_size = 1, shape = None, shuffle = True):
	""" Fetches images from data_dir in a generative fashion, without overloading
	the RAM. 
	Args:
		batch_size: An int specifying the batch size of images returned.
		shape: A tuple / list of ints specifying (height, width) of returned images.
		shuffle: A bool specifying whether to shuffle dataset randomly.
	Returns:
		a list of numpy arrays representing images.
	"""
	files = []
	for root, directories, filenames in os.walk(data_dir):
		for filename in filenames:
			# if filename[0] != '.':
			files.append(os.path.join(root, filename))
	if shuffle:
		np.random.shuffle(files)
	for i in range(0, len(files)-batch_size+1, batch_size):
		M = []
		for file_name in files[i:i+batch_size]:
			M_curr = imread(file_name)
			if shape:
				M_curr = imresize(M_curr, (shape[0], shape[1]), interp = 'lanczos')
			M.append(M_curr)
		yield M

