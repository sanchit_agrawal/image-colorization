import tensorflow as tf

def leaky_relu(x, alpha = 0.01):
	return tf.maximum(alpha * x, x)

def batch_norm(x, is_training, decay = 0.9):
	return tf.contrib.layers.batch_norm(x, decay = decay, scale = False, center = False, is_training = is_training)

def conv(x, W, b = None, strides = (2, 2), padding = 'SAME', activation = tf.nn.relu):
	x = tf.nn.conv2d(x, W, strides=[1, strides[0], strides[1], 1], padding = padding)
	if b:
		x = tf.nn.bias_add(x, b)
	return x

def deconv(x, W, output_shape, b = None, strides = (2, 2), padding = 'SAME'):
	x = tf.nn.conv2d_transpose(x, W, output_shape = output_shape, strides = [1, strides[0], strides[1], 1], padding = padding)
	x = tf.reshape(x, output_shape)
	if b:
		x = tf.nn.bias_add(x, b)
	return x