import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from data_input import get_images
from ops import *

class Model:

# Put other parameters.
	def __init__(self,
		image_shape = (256, 256),
		n_channels_G = (1, 32, 64, 128, 256),
		filter_sizes_G = (5, 5, 5, 5),
		strides_G = (2, 2, 2, 2),
		n_channels_D = (3, 32, 64, 128, 256),
		filter_sizes_D = (5, 5, 5, 5),
		strides_D = (2, 2, 2, 2),
		apply_batch_norm = True): 
		self.GW = []
		self.Gb = []
		self.GW_deconv = []
		self.Gb_deconv = []
		self.DW = []
		self.Db = []
		self.height = image_shape[0]
		self.width = image_shape[1]
		self.n_channels_G = n_channels_G
		self.filter_sizes_G = filter_sizes_G
		self.strides_G = strides_G
		self.n_channels_D = n_channels_D
		self.filter_sizes_D = filter_sizes_D
		self.strides_D = strides_D
		self.apply_batch_norm = apply_batch_norm
		# input is used for both, the generator and the discriminator
		self.input = tf.placeholder(tf.uint8, shape = (None, self.height, self.width, 3))
		self.float_input = tf.image.convert_image_dtype(self.input, tf.float32)
		self.hsv_input = tf.image.rgb_to_hsv(self.float_input)
		# labels are used for the discriminator
		self.labels = tf.placeholder(tf.int32, shape = (None))
		if self.apply_batch_norm:
			self.is_training = tf.placeholder(tf.bool, shape = ())
		else:
			self.is_training = None
		self.prepare_generator_weights()
		self.prepare_discriminator_weights()
		self.prepare_generator_updates()
		self.prepare_discriminator_updates()
		self.init_weights = tf.global_variables_initializer()

	def prepare_generator_weights(self):
		self.GW = [tf.get_variable('GW_{}'.format(i),
			shape = [self.filter_sizes_G[i], self.filter_sizes_G[i],
				self.n_channels_G[i], self.n_channels_G[i+1]],
			initializer = tf.contrib.layers.xavier_initializer()) 
		for i in range(len(self.filter_sizes_G))]

		self.GW_deconv = [tf.get_variable('GW_deconv_{}'.format(i),
			shape = [self.filter_sizes_G[-i-1], self.filter_sizes_G[-i-1],
				self.n_channels_G[-i-2], self.n_channels_G[-i-1]],
			initializer = tf.contrib.layers.xavier_initializer()) 
		for i in range(len(self.filter_sizes_G)-1)]

		self.GW_deconv.append(tf.get_variable('GW_deconv_{}'.format(len(self.filter_sizes_G)-1),
			shape = [self.filter_sizes_G[0], self.filter_sizes_G[0], 2, self.n_channels_G[1]],
			initializer = tf.contrib.layers.xavier_initializer()))

		if not self.apply_batch_norm:
			self.Gb = [tf.get_variable('Gb_{}'.format(i), 
				shape = [self.n_channels_G[i+1]],
				initializer = tf.constant_initializer(0.1)) 
			for i in range(len(self.filter_sizes_G))]

			self.Gb_deconv = [tf.get_variable('Gb_deconv_{}'.format(i),
				shape = [self.n_channels_G[-i-2]],
				initializer = tf.constant_initializer(0.1))
			for i in range(len(self.filter_sizes_G)-1)]
		else:
			self.Gb = [None] * len(self.filter_sizes_G)

			self.Gb_deconv = [None] * (len(self.filter_sizes_G)-1)
		
		# NOTE: No batch norm applied to generator output
		self.Gb_deconv.append(tf.get_variable('Gb_deconv_{}'.format(len(self.filter_sizes_G)-1),
			shape = [2], initializer = tf.constant_initializer(0.1)))

	def prepare_discriminator_weights(self):
		self.DW = [tf.get_variable('DW_{}'.format(i),
			shape = [self.filter_sizes_D[i], self.filter_sizes_D[i],
				self.n_channels_D[i], self.n_channels_D[i+1]],
			initializer = tf.contrib.layers.xavier_initializer()) 
		for i in range(len(self.filter_sizes_D))]

		self.Db = [tf.get_variable('Db_{}'.format(i), 
			shape = [self.n_channels_D[i+1]],
			initializer = tf.constant_initializer(0.1)) 
		if not self.apply_batch_norm or i == 0 else None 
		for i in range(len(self.filter_sizes_D))]

		final_shape = (float(self.height), float(self.width))
		for s in self.strides_D:
			final_shape = (np.ceil(final_shape[0]/s), np.ceil(final_shape[1]/s))

		self.DW.append(tf.get_variable('DW_fully_connected',
			shape = [int(final_shape[0]) * int(final_shape[1]) * self.n_channels_D[-1], 1],
			initializer = tf.contrib.layers.xavier_initializer()))
		
		if not self.apply_batch_norm:
			self.Db.append(tf.get_variable('Db_fully_connected', shape = (),
				initializer = tf.constant_initializer(0.1)))
		else:
			self.Db.append(None)

	def generator(self, images):
		curr_input = tf.expand_dims(images, axis = -1)
		shapes = [(tf.shape(curr_input)[0], self.height, self.width, 1)]
		# Convolution
		for i in range(len(self.filter_sizes_G)):
			curr_input = conv(curr_input, W = self.GW[i], b = self.Gb[i],
				strides = (self.strides_G[i], self.strides_G[i]))
			if self.apply_batch_norm:
				curr_input = batch_norm(curr_input, self.is_training)
			curr_input = tf.nn.relu(curr_input)
			shapes.append((tf.shape(curr_input)[0],
				np.ceil(float(shapes[-1][1])/self.strides_G[i]),
				np.ceil(float(shapes[-1][2])/self.strides_G[i]),
				self.n_channels_G[i+1]))
		shapes.reverse()
		# Deconvolution
		for i in range(len(self.filter_sizes_G)-1):
			curr_input = deconv(curr_input, W = self.GW_deconv[i], b = self.Gb_deconv[i],
				output_shape = [shapes[i+1][0], shapes[i+1][1],
					shapes[i+1][2], shapes[i+1][3]],
				strides = (self.strides_G[-i-1], self.strides_G[-i-1]))
			if self.apply_batch_norm:
				curr_input = batch_norm(curr_input, self.is_training)
			curr_input = tf.nn.relu(curr_input)
		output = deconv(curr_input, W = self.GW_deconv[-1], b = self.Gb_deconv[-1],
			output_shape = tf.pack([shapes[-1][0], shapes[-1][1], shapes[-1][2], 2]),
			strides = (self.strides_G[0], self.strides_G[0]))
		output = tf.nn.sigmoid(output)
		output_channels = tf.unstack(output, axis = 3)
		return tf.stack([output_channels[0], output_channels[1], images], axis = 3)

	# output probability that the image is real.
	def discriminator(self, images):
		curr_input = images
		# Convolution
		for i in range(len(self.filter_sizes_D)):
			curr_input = conv(curr_input, W = self.DW[i], b = self.Db[i],
				strides = (self.strides_D[i], self.strides_D[i]))
			if self.apply_batch_norm:
				curr_input = batch_norm(curr_input, self.is_training)
			curr_input = leaky_relu(curr_input)
		curr_input = tf.reshape(curr_input, [tf.shape(images)[0], -1])
		curr_input = tf.matmul(curr_input, self.DW[-1])
		curr_input = tf.unstack(curr_input, axis = -1)[0]
		if not self.apply_batch_norm:
			curr_input = curr_input + self.Db[-1]
		else:
			curr_input = tf.reshape(curr_input, shape = [-1, 1, 1, 1])
			curr_input = batch_norm(curr_input, self.is_training)
			curr_input = tf.squeeze(curr_input, axis = [1, 2, 3])
		return tf.nn.sigmoid(curr_input)
	
	def generator_loss(self, images):
		self.G = self.generator(images)
		# RGB_G is not part of the model. It's just used to return the RGB version.
		self.RGB_G = tf.image.hsv_to_rgb(self.G)
		self.zeros_dims = tf.shape(self.G)[0]
		self.temp_labels = tf.fill([self.zeros_dims], 0)
		self.DG, loss = self.discriminator_loss(self.G, self.temp_labels)
		return -loss

	def discriminator_loss(self, images, labels):
		D_pred = self.discriminator(images)
		D_distribution = tf.stack([1-D_pred, D_pred], axis = 1)
		one_hot_labels = tf.one_hot(labels, 2)
		D_loss = -tf.reduce_mean(tf.reduce_sum(one_hot_labels * tf.log(D_distribution), 1))
		return D_distribution, D_loss

	def prepare_generator_updates(self):
		# During inferencing, feed grayscale_images (reduced to lie in [0,1])
		self.grayscale_images = tf.unstack(self.hsv_input, axis = 3)[2]
		self.G_loss = self.generator_loss(self.grayscale_images)
		self.G_optimizer = tf.train.AdamOptimizer()
		self.G_train_step = self.G_optimizer.minimize(self.G_loss,
			var_list = self.GW + [b for b in self.Gb if b] + \
			self.GW_deconv + [b for b in self.Gb_deconv if b])

	def prepare_discriminator_updates(self):
		self.D_prob, self.D_loss = self.discriminator_loss(self.hsv_input, self.labels)
		self.D_optimizer = tf.train.GradientDescentOptimizer(0.001)
		self.D_train_step = self.D_optimizer.minimize(self.D_loss,
			var_list = self.DW + [b for b in self.Db if b])